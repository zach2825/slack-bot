<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(['id' => '1'], [
            'email' => 'admin@admin.com',
            'name' => 'admin',
            'password' => bcrypt('admin'),
        ]);

        $this->command->info('Generated 1 user with email "admin@admin.com" and password "admin"');
    }
}
