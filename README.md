# Send slack messages and eventually other things like email, sms, and maybe computer alerts. 

This s basic, for now. Eventually I'll add more

## How to use this.
I setup an alias in my ~/.bash_aliases file to call this command but you can run 

/usr/bin/php /var/www/slack-bot/artisan send:slack-message "Your message here" 

## get up and running

### make a database. Eventually i plan to add more stuff like analytics and whatnot for now the system adds new messages to a queue that is sent out using supervisord or something
    mysql -uroot -p -e"create database some_database";

that is kind of a lie. right now it just sends out the message without a queue. eventually it'll queue it up

### copy the .env.example to .env
set the database credentials and slack web hook url. google to see hwo to get that url

### run composer install

### run php artisan migrate --seed
the --seed will create the one main user. 
the email is **admin@admin.com** and password is **admin**

### install and configure supervisord

Sample supervisor config file
[installing-and-configuring-supervisor-on-ubuntu-16-04][https://www.vultr.com/docs/installing-and-configuring-supervisor-on-ubuntu-16-04]

    [program:slack-bot-messages-queue]
    process_name=%(program_name)s_%(process_num)02d
    command=php /var/www/personal-work/slack-bot/artisan queue:work --tries=3 --delay=2 --daemon
    user=zach2825
    autostart=true
    autorestart=true
    numprocs=1
    redirect_stderr=true
    stdout_logfile=/var/www/personal-work/slack-bot/storage/logs/test.log
