<?php

namespace App\Console\Commands;

use App\Notifications\GeneralSlackMessageNotification;
use App\User;
use Illuminate\Console\Command;

class SendSlackMessageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:slack-message {message : the message text to send to slack}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a message to slack';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var User $user */
        $user = User::find(1);

        $user->notify(new GeneralSlackMessageNotification($this->argument('message')));

        return 0; // everything is ok.
    }
}
